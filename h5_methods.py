
from pathlib import Path
import os
import subprocess

def path_to_hdf5(path_str=str(), wildcards='*.edf', output_path=str(), rewrite=False, printable=True):
    """
    Runs a bash script to generate an HDF5 file upon searched files inside a directory
    
    Parameters:
    path_str (string) : path to the folder where search files
    wildcards (string) : wildcards with asterisks (*) to filter files
    output_path (string) : path to locate the output .h5 file, default is path_str
    
    Returns:
    None
    
    """
    path_folder = Path(path_str)
    
    # Build the output filename
    if not output_path:
        output_path = path_folder
    else:
        output_path = Path(output_path)
    path_h5 = output_path.joinpath(f"{output_path.name}.h5")
    
    # Check if the output filename exists already
    if path_h5.exists() and not rewrite:
        if printable:
            print(f"The file {str(path_h5)} already exists. Method exits.\n")
        return        
    
    # Method bash to silx convert
    bash_file = str(Path.cwd().joinpath('silx_convert.sh'))
    # Allow to execute the bash file
    cmd_permission = f"chmod 777 -R {bash_file}"
    os.system(cmd_permission)
    
    try:
        subprocess.call([bash_file, path_str, wildcards, str(path_h5)], stdout=subprocess.PIPE)
        if printable:
            print(f"The file {str(path_h5)} has been created succesfully.\n")
    except subprocess.CalledProcessError:
        if printable:
            print(f"The bash file could not be run. Check the arguments.\n")
            
            
            
            
def path_to_fabioserie(path_str=str(), wildcards='.*edf'):
    """
    Finds files inside the path_string and stacks them into a fabio series
    
    Parameters:
    path_str (string) : path to the folder where search files
    wildcards (string) : wildcards with asterisks (*) to filter files
    
    Returns:
    FileSeries : stacked files in format FileSeries from FabIO module
    
    """
    path = Path(path_str)
    list_edfs = list(Path.glob(path, wildcards))
    list_edfs.sort()
    fabio_serie = FileSeries(
        filenames=list_edfs,
    )
    return fabio_serie